require('./app/logger.js');
process.title = 'audio-convert-api';

let config = require('./config.js');
let bodyParser = require('body-parser');
let express = require('express');
let app = express();
// var http = require('http').Server(app);
let routes = require('./api/routes/audioProcessRoutes.js'); //importing route
let port = config.PORT || 8080;

// CORS config
app.use(function(req, res, next) {
  if ('OPTIONS' == req.method) {
    Logger.info('OPTIONS request');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.send(200);
  }
  else {
    next();
  }
});

// Auth middleware
app.use(function (req, res, next) {
  let TOKEN_KEY = 'token=';
  let TOKEN_REGEX = /^Token /;
  let auth = req.headers.authorization;
  if (auth && auth.match(TOKEN_REGEX)) {
    let token = auth.split(TOKEN_REGEX)[1]
    if (token && token.trim() == config.AUTH_TOKEN) {
      next();
    } else {
      res.status(401).send("Unauthenticated");
    }
  } else {
    res.status(401).send("Unauthenticated");
  }
});



var jsonParser = bodyParser.json({limit:1024*1024*100, type:'application/json'});
var urlencodedParser = bodyParser.urlencoded({ extended:true,limit:1024*1024*50,type:'application/x-www-form-urlencoding' })
app.use(jsonParser);
app.use(urlencodedParser);

routes(app); //register the route

app.listen(port);

Logger.info('Audio Processor RESTful API server started on: ' + port);