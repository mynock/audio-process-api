'use strict';

let redis = require('redis');
let config = require('../../config.js');
let bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype)

var redisClient = redis.createClient(config.REDIS_PORT, config.REDIS_HOST, config.REDIS_OPTIONS);
redisClient.on('connect', function() {
  Logger.info('Redis connected');
});

redisClient.on('error', function() {
  Logger.error('Redis error');
});

exports.queue_job = function(req, res) {
  Logger.info("Queueing a Job");
  let payload = JSON.stringify(req.body);
  redisClient.rpush('AudioJobs', payload);
  res.status(200).send("Thanks!");
};

exports.list_jobs = function(req, res) {
  Logger.info("Listing Jobs")
  let key = "AudioJobs";
  let ret = {
    total_length: 0,
    jobs: []
  }
  let allJobs = [];

  redisClient.llen(key, function(err, length) {
    Logger.info("Length: " + length);
    ret.total_length = length;
    redisClient.lrange(key, 0, -1, function (_err, jobData) {
      if(err || _err) {
        Logger.error("Error!");
      }
      else {
        allJobs = jobData.map(function(item, idx) { return JSON.parse(item) });
        ret.jobs = allJobs;
        res.status(200).send(ret);
      }
    });
  });
};