'use strict';

module.exports = function(app) {
  var audioProcessor = require('../controllers/audioProcessController');

  app.route('/process')
    .post(audioProcessor.queue_job);

  app.route('/jobs')
    .get(audioProcessor.list_jobs);

};