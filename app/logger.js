winston = require('winston');
global.Logger = new winston.Logger({
  transports: [
    new (winston.transports.Console)({'timestamp':true, level: 'debug'})
  ]
});
