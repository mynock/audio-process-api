module.exports = {
  PORT: process.env.PORT || 8080,
  REDIS_PORT: process.env.REDIS_PORT || 6380, // Run on 6380 b/c that is the port mapped in docker-compose
  REDIS_HOST: process.env.REDIS_HOST || '0.0.0.0',
  REDIS_OPTIONS: {},
  AUTH_TOKEN: process.env.AUTH_TOKEN || 'secret_token_local_testing'
};
